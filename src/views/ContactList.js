import {React, Component } from 'react'
import axios from 'axios'
import {NavLink} from 'react-router-dom'

export default class ContactList extends Component {
    constructor(){
        super()
        this.state = {contact:[]}
    }
    getContact = async () => {
         let response = await axios.get('http://localhost:3000/listcontact')
        //  let response = await axios.get('https://jsonplaceholder.typicode.com/users')
        this.setState({
            contact: response.data
        })
    }
    componentDidMount(){
        this.getContact()
    }
    handleDelete = (data) => {
        axios.delete(`http://localhost:3000/contact/${data}`).then(res => {
            alert('data berhasil di hapus');
            this.getContact()
        })
    };
    render() {
    const {contact} = this.state
    //  console.log(contact);
    //   return false;
        return (
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-6 mt-5">
                        <NavLink exact className="btn btn-info" to="/form-contact">Tambah Kontak</NavLink>
                        <table className="table">
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>No Tlp</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    contact.map((contacts, index) =>{
                                        return(
                                            <tr key={index}>
                                                <td>{contacts.nama}</td>
                                                <td>{contacts.email}</td>
                                                <td>{contacts.nohp}</td>
                                                <td><button className="btn btn-danger" onClick={() => this.handleDelete(contacts.id)}>Hapus</button></td>
                                            </tr>
                                        )
                                    })              
                                }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )
    }

}
