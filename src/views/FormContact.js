import React, { Component } from 'react'
import axios from 'axios'

export default class FormContact extends Component {
    constructor(){
        super();
        this.state = {
            nama: '',
            email: '',
            nohp: ''
        };
    }
    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    }
    handleSubmit = (event) => {
        event.preventDefault();

        axios.post('http://localhost:3000/addcontact', {
            nama: this.state.nama,
            email: this.state.email,
            nohp: this.state.nohp
        }).then(res => {
            console.log(res.data);
            // return res.json();
            alert('Data berhasil di tambahkan!');
            window.location = "/contact"
        })
    }
    render() {
        return (
            <div className="container mt-5">
                <div className="row">
                <div className="col-md-4">
                    <div className="card">
                    <div className="card-header">Form Tambah Kontak
                        <div className="card-body">
                            <form onSubmit={this.handleSubmit}>
                            <div className="mb-4">
                                <label htmlFor="nama" className="form-label">Nama Lengkap</label>
                                <input type="text" name="nama" value={this.state.value} onChange={this.handleChange} id="nama" className="form-control" />
                                </div>
                                <div className="mb-4">
                                <label htmlFor="email" className="form-label">Email</label>
                                <input type="text" name="email" value={this.state.value} onChange={this.handleChange} id="email" className="form-control" />
                                </div>
                                <div className="mb-4">
                                <label htmlFor="nohp" className="form-label">No HP</label>
                                <input type="number" name="nohp" value={this.state.value} onChange={this.handleChange} id="nohp" className="form-control" />
                                </div>
                                <button type="submit" className="btn btn-warning">Simpan</button>
                            </form>
                        </div>
                </div>
                </div>
            </div>
            </div>
            </div>
        )
    }
}
